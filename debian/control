Source: wmbattery
Section: x11
Priority: optional
Build-Depends: debhelper-compat (= 12),
               libupower-glib-dev,
               libxext-dev,
               libxpm-dev
Maintainer: Debian Window Maker Team <team+wmaker@tracker.debian.org>
Uploaders: Doug Torrance <dtorrance@piedmont.edu>,
           Jeremy Sowden <jeremy@azazel.net>
Rules-Requires-Root: no
Standards-Version: 4.4.1
Homepage: https://www.dockapps.net/wmbattery
Vcs-Browser: https://salsa.debian.org/wmaker-team/wmbattery
Vcs-Git: https://salsa.debian.org/wmaker-team/wmbattery.git

Package: wmbattery
Architecture: any
Depends: upower, ${misc:Depends}, ${shlibs:Depends}
Suggests: wmaker
Description: display laptop battery info, dockable in WindowMaker
 wmbattery displays the status of your laptop's battery in a small icon.
 This includes if it is plugged in, if the battery is charging, how many
 minutes of battery life remain, and battery status (high - green, low -
 yellow, or critical - red).
 .
 There's nothing in the program that makes it require WindowMaker, except
 maybe the look. It can be docked in WindowMaker or AfterStep's dock.
 .
 wmbattery supports multi-battery machines, and can estimate how long
 it will take the battery to finish charging or discharging.
